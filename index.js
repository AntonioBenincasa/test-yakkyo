import { connect } from 'mongoose'
import bodyParser from 'body-parser'
import express from 'express'
import UserModel from './UserModel'
import cors from 'cors'

//si connette mongoose al db mongo del progetto
connect('mongodb://test:yakkyo@ds135069.mlab.com:35069/test-yakkyo', { useNewUrlParser: true })

//viene istanziata un'applicazione express
const app = express()
//viene abilitato il middleware cors, che non permette ai client con domini che non sono autorizzati di accedere alla risorsa richiesta
app.use(cors())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

app.get('/', (req, res) => {
  res.send('Just a test')
})

app.get('/users', (req, res) => {
  //vengono recuperati tutti gli utenti dal db
  UserModel.find((err, results) => {
    res.send(results)
  })
})

app.post('/users', (req, res) => {
  //viene creato un nuovo modello dell'utente
  let user = new UserModel()
  //vengono recuperati i dati dell'utente dal body della richiesta
  user.email = req.body.email
  user.firstName = req.body.firstName
  user.lastName = req.body.lastName
  user.password = req.body.password
  //viene salvato il nuovo utente nella collection
  user.save((err, newUser) => {
    if (err) res.send(err)
    else res.send(newUser)
  })
})

app.delete('/users/:id', (req, res) => {
  //viene recuperato l'id dai parametri del route
  const id = req.params.id;
  //viene eliminato l'utente in base all'id e viene mostrato o nel caso in cui l'utente non esista o per qualunque altro problema viene mostrato un messaggio di errore
  UserModel.findByIdAndDelete(id, (err, deletedUser) => {
    if(err) res.send(err)
    else res.send(deletedUser);
  })
});

app.get('/users/:id', (req, res) => {
  //viene recuperato l'id dai parametri del route
  const id = req.params.id;
  //viene mostrato l'utente o in caso di errore (es. l'utente non esiste) viene mostrato un errore
  UserModel.findById(id, (err, user) => {
    if(err) res.send(err)
    else res.send(user);
  })
});

//il server viene messo in ascolto sulla porta 8080
app.listen(8080, () => console.log('Example app listening on port 8080!'))
