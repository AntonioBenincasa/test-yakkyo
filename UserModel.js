import { Schema, model } from 'mongoose'

//Definizione dello schema dell'utente
const UserSchema = new Schema({
  email: { type: String, lowercase: true, trim: true, required: true, unique: true },
  password: { type: String, required: true, select: false },
  firstName: { type: String, required: true },
  lastName: { type: String, required: true }
})

//si crea un modello che permette di interagire con la collection degli utenti
export default model('User', UserSchema)
